import React, {Component} from 'react'
import {Linking, View} from 'react-native';
import ProfileHeader from "../components/profile/ProfileHeader";
import axios from 'axios'
import SettingsItems from "../components/settings/SettingsItems";
import Spinner from 'react-native-loading-spinner-overlay';

class AccountScreen extends Component {

    state = {userProfile: [], visible: false};

    componentWillMount() {
        this.setState({visible: true});

        axios.get('https://private-d15747-demoreact1.apiary-mock.com/api/v1/user')
            .then(response => {
                this.setState({userProfile: response.data.user})
                this.setState({visible: false});
            });
    }

    renderHeader(){
        return <ProfileHeader user={this.state.userProfile} />
    }

    render() {

        return(
            <View style={{backgroundColor: '#0B1205',flex:1}}>
                {/*<StatusBarBackground style={{backgroundColor:'#000000'}}/>*/}

                <Spinner visible={this.state.visible}
                         cancelable = {false}
                         animation = {'fade'}
                         size = {'large'}
                         overlayColor = {'rgba(1, 5, 1, 0.8)'}
                         textContent={"Loading..."}
                         textStyle={{color: '#FFF'}} />

                <View>
                    {this.renderHeader()}
                </View>

                <View style={{
                    flex: 1,
                    justifyContent: 'space-between',
                    alignItems: 'stretch',
                }}>

                    <SettingsItems
                        settingItemText={'My Wishlist'}
                        iconUri={require('../assets/icons/ic_favorite_border_white.png')}
                        isPress={() => this.props.navigation.navigate('Detail', 'Wishlist Setting')}/>
                    <SettingsItems
                        settingItemText={'My Orders'}
                        iconUri={require('../assets/icons/ic_local_mall_white.png')}
                        isPress={() => this.props.navigation.navigate('Detail','Order Setting')}/>
                    <SettingsItems
                        settingItemText={'My Address Book'}
                        iconUri={require('../assets/icons/ic_add_location_white.png')}
                        isPress={() => this.props.navigation.navigate('Detail', 'Address Setting')}/>
                    <SettingsItems
                        settingItemText={'My eGift Cards'}
                        iconUri={require('../assets/icons/ic_local_play_white.png')}
                        isPress={() => this.props.navigation.navigate('Detail', 'Giftcard Setting')}/>
                    <SettingsItems
                        settingItemText={'My Rewards'}
                        iconUri={require('../assets/icons/ic_card_giftcard_white.png')}
                        isPress={() => this.props.navigation.navigate('Detail', 'Reward Setting')}/>
                    <SettingsItems
                        settingItemText={'Notifications'}
                        iconUri={require('../assets/icons/ic_audiotrack_white.png')}
                        isPress={() => this.props.navigation.navigate('Detail', 'Notification Setting')}/>
                    <SettingsItems
                        settingItemText={'Help & Support'}
                        iconUri={require('../assets/icons/ic_help_outline_white.png')}
                        isPress={() => Linking.openURL('http://www.goshop.com.my')}/>
                    <SettingsItems
                        settingItemText={'Settings'}
                        iconUri={require('../assets/icons/ic_settings_white.png')}
                        isPress={() => this.props.navigation.navigate('Detail', 'Setting Detail')}/>

                </View>

            </View>
        );
    };

}

export default AccountScreen;
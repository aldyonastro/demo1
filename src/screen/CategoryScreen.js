import React from 'react';
import { Text, View } from 'react-native';

class CategoryScreen extends React.Component {
    render() {
        return (
            <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
                <Text>Category Screen</Text>
            </View>
        );
    }
}

export default CategoryScreen;

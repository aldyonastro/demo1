import React from 'react';
import { Text, View } from 'react-native';

class ShoppingCartScreen extends React.Component {
    render() {
        return (
            <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
                <Text>Shopping Cart Screen</Text>
            </View>
        );
    }
}

export default ShoppingCartScreen;

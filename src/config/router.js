import React from 'react';
import { Image } from 'react-native';
import { StackNavigator, TabNavigator } from 'react-navigation';
import { NavigationComponent } from 'react-native-material-bottom-navigation';
import HomeScreen from '../screen/HomeScreen';
import CategoryScreen from '../screen/CategoryScreen';
import LoyaltyScreen from '../screen/LoyaltyScreen';
import ShoppingCartScreen from '../screen/ShoppingCartScreen';
import AccountScreen from '../screen/AccountScreen';
import DetailScreen from '../screen/DetailScreen';

export const BottomBarNav = ({
    mode: 'modal',
    tabBarComponent: NavigationComponent,
    tabBarPosition: 'bottom',
    tabBarOptions: {
        bottomNavigationOptions: {
            labelColor: 'white',
            rippleColor: 'white',
            shifting: false,
            tabs: {
                Home: {
                    barBackgroundColor: '#1D3244',
                },
                Category: {
                    barBackgroundColor: '#1D3244',
                },
                Loyalty: {
                    barBackgroundColor: '#1D3244',
                },
                ShoppingCart: {
                    barBackgroundColor: '#1D3244',
                },
                Account: {
                    barBackgroundColor: '#1D3244',
                },
            },
        },
    },
});

export const AccountStack = StackNavigator(
    {
        Settings: {
            screen: AccountScreen,
            navigationOptions: {
                title: '',
                header: false,
                tabBarIcon: () =>
                    <Image
                        style={{ width: 24, height: 24 }}
                        source={require('../assets/icons/ic_person_white.png')}
                />,
            },
        },
        Detail: {
            screen: DetailScreen,
            navigationOptions: ({ navigation }) => ({
                title: `${navigation.state.params}`,
            }),
        },
    },
    {
        BottomBarNav,
    },
);

export const MainTabs = TabNavigator(
    {
        Home: {
            screen: HomeScreen,
            navigationOptions: {
                tabBarLabel: 'Home',
                tabBarIcon: () =>
                    <Image
                        style={{ width: 24, height: 24 }}
                        source={require('../assets/icons/ic_home_white.png')}
                />,
            },
        },
        Category: {
            screen: CategoryScreen,
            navigationOptions: {
                tabBarLabel: 'Category',
                tabBarIcon: () =>
                    <Image
                    style={{ width: 24, height: 24 }}
                    source={require('../assets/icons/ic_view_list_white.png')}
                />,
            },
        },
        Loyalty: {
            screen: LoyaltyScreen,
            navigationOptions: {
                tabBarLabel: 'Loyalty',
                tabBarIcon: () =>
                    <Image
                    style={{ width: 24, height: 24 }}
                    source={require('../assets/icons/ic_stars_white.png')}
                />,
            },
        },
        ShoppingCart: {
            screen: ShoppingCartScreen,
            navigationOptions: {
                tabBarLabel: 'Cart',
                tabBarIcon: () =>
                    <Image
                    style={{ width: 24, height: 24 }}
                        source={require('../assets/icons/ic_shopping_cart_white.png')}
                />,
            },
        },
        Account: {
            screen: AccountStack,
            navigationOptions: {
                tabBarLabel: 'Account',
                tabBarIcon: () =>
                    <Image
                    style={{ width: 24, height: 24 }}
                        source={require('../assets/icons/ic_person_white.png')}
                />,
            },
        },
    },
    {
        tabBarComponent: NavigationComponent,
        tabBarPosition: 'bottom',
        tabBarOptions: {
            bottomNavigationOptions: {
                labelColor: 'white',
                rippleColor: 'white',
                shifting: false,
                tabs: {
                    Home: {
                        barBackgroundColor: '#1D3244',
                    },
                    Category: {
                        barBackgroundColor: '#1D3244',
                    },
                    Loyalty: {
                        barBackgroundColor: '#1D3244',
                    },
                    ShoppingCart: {
                        barBackgroundColor: '#1D3244',
                    },
                    Account: {
                        barBackgroundColor: '#1D3244',
                    },
                },
            },
        },
    },
);

export const Root = StackNavigator({
    Tabs: {
        screen: MainTabs,
    },
    Settings: {
        screen: AccountStack,
    },
}, {
    mode: 'modal',
    headerMode: 'none',
});

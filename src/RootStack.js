import HomeScreen from './screen/HomeScreen';
import { StackNavigator } from 'react-navigation';
import DetailScreen from './screen/DetailScreen';

const RootStack = StackNavigator(
    {
        Home: {
            screen: HomeScreen,
        },
        Details: {
            screen: DetailScreen,
        },
    },
    {
        initialRouteName: 'Home',
    },
);

export default RootStack;

import React from 'react';
import { Text, View, ImageBackground, Image } from 'react-native';

const ProfileHeader = ({ user }) => {
    const { name, imageUrl } = user;
    const {
        textStyleName,
        textStyleEditProfile,
        contentViewStyle,
        nameViewContainerStyle,
        avatarViewStyle,
        imageBgStyle,
        avatarImgStyle,
        rootViewStyle,
    } = customStyles;

    return (
        <View style={rootViewStyle}>
            <ImageBackground
                style={imageBgStyle}
                source={require('../../assets/background/bg1.jpg')}
            />
            <View style={contentViewStyle}>
                <View style={nameViewContainerStyle}>
                    <Text style={textStyleName}>{name}</Text>
                    <Text style={textStyleEditProfile}>Edit Profile</Text>
                </View>
                <View style={avatarViewStyle}>
                    <Image
                        style={avatarImgStyle}
                        source={{
                            uri: imageUrl,
                            cache: 'force-cache',
                            resizeMode: 'stretch',
                        }}
                    />
                </View>
            </View>
        </View>
    );
};

const customStyles = {
    rootViewStyle: {
        backgroundColor: 'transparent',
        height: 125,
        justifyContent: 'center',
        alignItems: 'center',
    },
    contentViewStyle: {
        flexDirection: 'row',
    },
    imageBgStyle: {
        position: 'absolute',
        left: 0,
        right: 0,
        top: 0,
        bottom: 0,
    },
    nameViewContainerStyle: {
        backgroundColor: 'transparent',
        paddingLeft: 24,
        flex: 2,
        justifyContent: 'center',
    },
    avatarViewStyle: {
        paddingTop: 8,
        backgroundColor: 'transparent',
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    textStyleName: {
        fontSize: 25,
        color: '#FFFFFF',
    },
    textStyleEditProfile: {
        fontSize: 16,
        color: '#FFFFFF',
    },
    avatarImgStyle: {
        width: 90,
        height: 90,
        borderRadius: 45,
    },
};

export default ProfileHeader;

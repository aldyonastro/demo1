import React, { Component } from 'react';
import { View, Text, Image, TouchableOpacity } from 'react-native';

const SettingsItems = (record) => {
    const { containerStyle, textStyle, iconStyle } = settingItemStyle;

    const { settingItemText, iconUri, isPress } = record;

    return (
        <TouchableOpacity onPress={isPress}>
            <View style={containerStyle}>
                <Text style={textStyle}>{settingItemText}</Text>

                <Image
                    style={iconStyle}
                    resizeMode="contain"
                    source={iconUri}
                />
            </View>
        </TouchableOpacity>
    );
};

const settingItemStyle = {
    containerStyle: {
        borderBottomWidth: 0,
        paddingLeft: 16,
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row',
        borderColor: '#ddd',
        position: 'relative',
        marginTop: 12,
        marginBottom: 12,
    },
    textStyle: {
        flex: 5,
        textAlign: 'left',
        justifyContent: 'center',
        alignItems: 'center',
        fontSize: 18,
        color: '#FFFFFF',
    },
    iconStyle: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'flex-end',
    },
};

export default SettingsItems;

import React, { Component } from 'react';
import { YellowBox } from 'react-native';
import { Root } from './src/config/router';

YellowBox.ignoreWarnings(['Warning: isMounted(...) is deprecated', 'Module RCTImageLoader']);

class App extends Component {
    render() {
        return <Root />;
    }
}


export default App;

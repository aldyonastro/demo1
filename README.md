
# README #  
 
Expo App  
  
  ![](https://i.imgur.com/ch8rR0l.png)
  
Your URL is: exp://ep-jkg.aldychris.demo1.exp.direct:80  
  
Instructions to open this project on a physical device  
Android devices: scan the above QR code.  
iOS devices: run exp send -s <your-phone-number-or-email> in this project directory in another terminal window to send the URL to your device.  
  
Instructions to open this project on a simulator  
If you already have the simulator installed, run exp ios or exp android in this project directory in another terminal window.